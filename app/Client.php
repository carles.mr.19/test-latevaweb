<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['nom', 'cognoms', 'data_naixement'];

    public function tractaments() {
        return $this->belongsToMany('App\Tractament');
    }

}
