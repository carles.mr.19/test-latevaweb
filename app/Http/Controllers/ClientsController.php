<?php

namespace App\Http\Controllers;

use App\Client;
use App\Tractament;
use Illuminate\Http\Request;

class ClientsController extends Controller {

    public function index() {
        $clients = Client::paginate(8);

        return view('clients.index')->with('clients',$clients);
    }

    public function create() {
        return view('clients.create');
    }

    public function store(Request $request) {

        $request->validate([
            'nom' => 'required',
            'cognoms' => 'required',
            'dataNaixament' => 'required'
        ]);

        $client = new Client();

        $client->nom = $request->input('nom');
        $client->cognoms = $request->input('cognoms');
        $client->data_naixement = $request->input('dataNaixament');

        $client->save();

        return redirect()->route('clients.index')->with('success', 'S\'ha afegit el client.');
    }

    public function show($id) {
        $client = Client::find($id);
        $tractaments = Tractament::all();

        return view('clients.show')->with('client', $client)->with('tractaments', $tractaments);
    }

    public function edit($id) {
        $client = Client::find($id);

        return view('clients.edit')->with('client', $client);
    }


    public function update(Request $request, $id) {

        $request->validate([
            'nom' => 'required',
            'cognoms' => 'required',
            'dataNaixament' => 'required'
        ]);

        $client = Client::find($id);

        $client->nom = $request->input('nom');
        $client->cognoms = $request->input('cognoms');
        $client->data_naixement = $request->input('dataNaixament');

        $client->save();

        return redirect()->route('clients.index')->with('success','S\'ha modificat el client.');
    }

    public function destroy($id) {
        $client = Client::find($id);

        $client->delete();


        return redirect()->route('clients.index')->with('success','S\'ha eliminat el client.');
    }

    public function search(Request $request) {
        $search = $request->get('search');

        $array = explode(" ", $request->get('search'));

        if(count($array) < 2){
            $posts = Client::where('nom', 'like', '%' . $search . '%')->orWhere('cognoms', 'like', '%' . $search . '%')->paginate(6);

        }else{
            $posts = Client::where('nom', 'like', '%' . $array[0] . '%')->orWhere('cognoms', 'like', '%' . $array[1] . '%')->paginate(6);
        }

        if(count($posts) == 0){
            return redirect()->route('clients.index')->with('success', 'No s\'ha trobat cap client');
        }

        return view('clients.index')->with('clients', $posts);
    }

    public function afegirTractament(Request $request){
        $id_cliente = $request->get('client_id');
        $id_tratamiento = $request->get('product_id');

        $client = Client::find($id_cliente);
        $tractaments = Tractament::all();

        $client->tractaments()->attach($id_tratamiento);

        return view('clients.show')->with('client', $client)->with('tractaments', $tractaments);
    }

}
