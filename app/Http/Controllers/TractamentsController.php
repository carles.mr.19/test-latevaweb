<?php

namespace App\Http\Controllers;

use App\Tractament;
use Illuminate\Http\Request;

class TractamentsController extends Controller {

    public function index() {
        $tractaments = Tractament::paginate(8);

        return view('tractaments.index')->with('tractaments',$tractaments);
    }

    public function create() {
        return view('tractaments.create');
    }

    public function store(Request $request) {

        $request->validate([
            'nom' => 'required',
            'descripcio' => 'required'
        ]);

        $tractament = new Tractament([
            'nom' => $request->input('nom'),
            'descripcio' => $request->input('descripcio'),
        ]);

        $tractament->save();

        return redirect()->route('tractaments.index')->with('success', 'S\'ha afegit el tractament.');
    }

    public function show($id)  {
        $tractament = Tractament::find($id);

        return view('tractaments.show')->with('tractament', $tractament);
    }

    public function edit($id) {
        $tractament = Tractament::find($id);

        return view('tractaments.edit')->with('tractament', $tractament);
    }

    public function update(Request $request, $id) {
        $tractament = Tractament::find($id);

        $tractament->nom = $request->input('nom');
        $tractament->descripcio = $request->input('descripcio');

        $tractament->save();

        return redirect()->route('tractaments.index')->with('success','S\'ha modificat el tractament.');
    }

    public function destroy($id) {
        $tractament = Tractament::find($id);

        $tractament->delete();

        return redirect()->route('tractaments.index')->with('success','S\'ha eliminat el tractament.');
    }

    public function search(Request $request) {
        $search = $request->get('search');

        $posts = Tractament::where('nom', 'like', '%' . $search . '%')->paginate(6);

        return view('tractaments.index')->with('tractaments', $posts);
    }

}
