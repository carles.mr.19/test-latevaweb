<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tractament extends Model
{
    protected $fillable = ['nom', 'descripcio'];

    public function clients() {
        return $this->belongsToMany('App\Client');
    }

}
