<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); })->name('welcome');

Route::get('search_client', 'ClientsController@search')->name('search_client');
Route::get('search_tractament', 'TractamentsController@search')->name('search_tractament');

Route::post('clients/afegir_tractament', 'ClientsController@afegirTractament')->name('afegirTractament');

Route::resource('clients', 'ClientsController');

Route::resource('tractaments', 'TractamentsController');
