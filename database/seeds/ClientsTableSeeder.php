<?php

use App\Client;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder {

    public function run(){
        $faker = Faker\Factory::create();

        for($i = 0; $i < 100; $i++) {
            Client::create([
                'nom' => $faker->name,
                'cognoms' => $faker->lastName ,
                'data_naixement' => $faker->date($format = 'Y-m-d', $max = 'now')
            ]);
        }
    }
}
