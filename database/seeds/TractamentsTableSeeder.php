<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

use App\Tractament;

class TractamentsTableSeeder extends Seeder {

    public function run() {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 100; $i++) {
            Tractament::create([
                'nom' => $faker->name,
                'descripcio' => $faker->text,

            ]);
        }
    }
}
