# latevaweb (Prova de Nivell)

Carles Miranda Rodriguez

## Requisits
- Laravel >= 5.7


## Instal·lació

#### Cloneu aquest projecte a la vostra carpeta de treball i obriu el directori:

```bash
git clone https://gitlab.com/carles.mr.19/test-latevaweb.git
```

#### Obrir la carpeta "test_latevaweb":

```bash
cd test-latevaweb
```

#### Descarregar Dependències del Projecte

Com les dependències del projecte les maneja **composer** hem d'executar la comanda:

```bash
composer install
```

### Important!

S'ha de crear una base de dades abans de seguir amb el procés de configuració del projecte.

#### Configura Entorn

La configuració de l'entorn es fa a l'arxiu **.env** però aquest arxiu no pot versionar segons les restriccions de l'arxiu **.gitignore**, igualment en el projecte hi ha un fitxer d'exemple **.env.example** hem de copiar amb la següent comanda:

```bash
cp .env.example .env
```

Després cal modificar els valors de les variables d'entorn per adequar la configuració al nostre entorn de desenvolupament, per exemple els paràmetres de connexió a la base de dades.

```bash
DB_CONNECTION=mysql
DB_HOST= //Direcció IP de la base de dades
DB_PORT=3306
DB_DATABASE= //Nom de la base de dades
DB_USERNAME= //Usuari
DB_PASSWORD= //Clau
```

#### Generar Clau de Seguretat de l'Aplicació

```bash
php artisan key:generate
```

####  Migrar la base de dades

El projecte ja té els models, migracions i seeders generats. Llavors l'únic que ens cal és executar la migració i executar la següent comanda:

```bash
php artisan migrate --seed
```

####  Iniciar l'aplicació

```bash
php artisan serve
```

Per últim haurem d'introduir la direcció "localhost:8000" al nostre navegador per tal de veure l'aplicació.