@extends('layout.main')

@section('content')

    <div class="float-right m-5">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCreate"> Afegir Client </button>

        <div class="mt-2">
            <form action="{{route('search_client')}}" method="GET">
                <div class="input-group">
                    <input class="form-control border-secondary py-2" type="search" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <h2 class="display-3">Clients</h2>
    <p class="lead">Llistat de Clients</p>

    <div class="col-sm-12">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Cognoms</th>
                <th scope="col">Data Naixement</th>
                <th scope="col">Accions<th>
            </tr>
        </thead>

        <tbody>
        @foreach($clients as $client)
            <tr>
                <th>{{$client->nom}}</th>
                <th>{{$client->cognoms}}</th>
                <th>{{date('d-m-Y', strtotime($client->data_naixement))}}</th>
                <th class="align-content-center">
                    <a href="{{route('clients.show', $client->id)}}" class="float-left">
                        <button type="button" class="btn-primary btn-sm">Mostrar</button>
                    </a>

                    <a href="{{route('clients.edit', $client->id)}}" class="float-left mr-2 ml-2">
                        <button type="button" class="btn-success btn-sm">Modificar</button>
                    </a>

                    <button data-toggle="modal" onclick="deleteData({{$client->id}})" data-target="#DeleteModal" class="btn-danger btn-sm">Eliminar</button>

                </th>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $clients->links() }}

    <a href="{{route('welcome')}}"><button type="button" class="btn btn-primary">Tornar</button></a>

    <!--    Modal Afegir  -->
    <div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Afegir Client</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('clients.store')}}">
                        @csrf
                        <div class="form-group">
                            <label for="nom">Nom:</label>
                            <input type="text" class="form-control" name="nom" id="nom" required>

                            <label for="cognoms">Cognoms</label>
                            <input type="text" class="form-control" name="cognoms" id="cognoms" required>

                            <label for="dataNaixament">Data Naixement:</label>
                            <input type="date" class="form-control" name="dataNaixament" id="dataNaixament" max="{{date('Y-m-d')}}" required>

                            <button type="submit" class="btn btn-primary mt-2 float-right">Afegir</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!--    /Modal Afegir  -->

    <!--    Modal Eliminar  -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <form action="" id="deleteForm" method="post">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title text-center" style="color:white;">Eliminar Client</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf @method('DELETE')
                        <p class="text-center">Estàs segur que vols eliminar el client?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Eliminar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function deleteData(id) {
            var id = id;
            var url = '{{ route("clients.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit() {
            $("#deleteForm").submit();
        }
    </script>
    <!--    /Modal Eliminar  -->
@stop
