@extends('layout.main')

@section('content')
    <h1>Afegir Client</h1>

    <div class="row justify-content-center">
        <div class="col">
            <form method="post" action="{{route('clients.store')}}">
                @csrf
                <div class="form-group">
                    <label for="nom">Nom:</label>
                    <input type="text" class="form-control" name="nom" id="nom" required>

                    <label for="cognoms">Cognoms</label>
                    <input type="text" class="form-control" name="cognoms" id="cognoms" required>

                    <label for="dataNaixament">Data Naixement:</label>
                    <input type="date" class="form-control" name="dataNaixament" id="dataNaixament" required>

                    <button type="submit" class="btn btn-primary mt-2">Afegir</button>
                </div>
            </form>
        </div>
    </div>
@stop