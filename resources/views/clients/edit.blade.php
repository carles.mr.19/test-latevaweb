@extends('layout.main')

@section('content')
    <h1>Editar Client</h1>

    <div class="row justify-content-center">
        <div class="col">
            <form action="{{route('clients.update', ['client' => $client->id])}}" method="POST">
                @csrf {{method_field('PUT')}}
                <div class="form-group">
                    <label for="nom">Nom:</label>
                    <input type="text" class="form-control" name="nom" id="nom" value="{{$client->nom}}" required>

                    <label for="cognoms">Cognoms</label>
                    <input type="text" class="form-control" name="cognoms" id="cognoms" value="{{$client->cognoms}}" required>

                    <label for="dataNaixament">Data Naixement:</label>
                    <input type="date" class="form-control" name="dataNaixament" id="dataNaixament" value="{{$client->data_naixement}}" max="{{date('Y-m-d')}}" required>

                    <a href="{{route('clients.index')}}"><button type="button" class="btn btn-primary mt-2">Tornar</button></a>
                    <button type="submit" class="btn btn-primary mt-2">Modificar</button>
                </div>
            </form>
        </div>
    </div>
@stop