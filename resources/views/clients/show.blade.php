@extends('layout.main')

@section('content')

    <div class="float-right m-5">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> Afegir Tractament </button>
    </div>

    <h2 class="display-3">{{$client->nom}} {{$client->cognoms}}</h2>

    <table class="table table-striped" id="tablaAutors">
        <thead>
        <tr>
            <th scope="col">Tractament</th>
        </tr>
        </thead>

        <tbody>
            @foreach($client->tractaments as $tractament)
                <tr>
                    <td>{{$tractament->nom}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <a href="{{route('clients.index')}}"><button type="button" class="btn btn-primary mt-2">Tornar</button></a>

    <!-- Modal Afegir Tractament -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Afegir Tractament</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('afegirTractament')}}" method="POST">
                        @csrf
                        <select class="form-control" name="product_id">
                            <option>Selecciona Tractament</option>
                            @foreach ($tractaments as $tractament)
                                <option value="{{ $tractament->id }}">
                                    {{ $tractament->nom}}
                                </option>
                            @endforeach
                        </select>

                        <input type="hidden" id="client_id" name="client_id" value="{{$client->id}}">

                        <div class="mt-2">
                            <button type="submit" class="btn btn-primary float-right">Afegir</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Modal Afegir Tractament -->

@stop
