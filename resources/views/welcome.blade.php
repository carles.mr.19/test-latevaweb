@extends('layout.main')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Prova de Nivell
            </div>

            <div class="links">
                <a href="{{route('clients.index')}}">Clients</a>
                <a href="{{route('tractaments.index')}}">Tractaments</a>
            </div>
        </div>
    </div>
@stop
