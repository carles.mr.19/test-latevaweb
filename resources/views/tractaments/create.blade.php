@extends('layout.main')

@section('content')
    <h1>Afegir Tractament</h1>

    <div class="row justify-content-center">
        <div class="col">
            <form method="post" action="{{route('tractaments.store')}}">
                @csrf
                <div class="form-group">
                    <label for="nom">Nom:</label>
                    <input type="text" class="form-control" name="nom" id="nom" required>

                    <label for="descripcio">Descripcio: </label>
                    <textarea class="form-control rounded-0" rows="3" name="descripcio" id="descripcio"></textarea>

                    <button type="submit" class="btn btn-primary">Afegir</button>
                </div>
            </form>
        </div>
    </div>
@stop