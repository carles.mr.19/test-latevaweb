@extends('layout.main')

@section('content')

    <div class="float-right m-5">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> Afegir Tractament </button>

        <div class="mt-2">
            <form action="{{route('search_tractament')}}" method="GET">
                <div class="input-group">
                    <input class="form-control border-secondary py-2" type="search" name="search">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <h2 class="display-3">Tractaments</h2>
    <p class="lead">Llistat de Tractaments</p>

    <div class="col-sm-12">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Nom</th>
            <th scope="col">Descripcio</th>
            <th scope="col">Accions<th>
        </tr>
        </thead>

        <tbody>
        @foreach($tractaments as $tractament)
            <tr>
                <th>{{$tractament->nom}}</th>
                <th>{{\Str::limit($tractament->descripcio)}}</th>
                <th class="align-content-center">
                    <a href="{{route('tractaments.show', $tractament->id)}}" class="float-left">
                        <button type="button" class="btn-primary btn-sm">Mostrar</button>
                    </a>

                    <a href="{{route('tractaments.edit', $tractament->id)}}" class="float-left mr-2 ml-2">
                        <button type="button" class="btn-success btn-sm">Modificar</button>
                    </a>

                    <button data-toggle="modal" onclick="deleteData({{$tractament->id}})" data-target="#DeleteModal" class="btn-danger btn-sm">Eliminar</button>

                </th>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $tractaments->links() }}

    <a href="{{route('welcome')}}"><button type="button" class="btn btn-primary">Tornar</button></a>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Afegir Tractament</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('tractaments.store')}}">
                        @csrf
                        <div class="form-group">
                            <label for="nom">Nom:</label>
                            <input type="text" class="form-control" name="nom" id="nom" required>

                            <label for="descripcio">Descripcion: </label>
                            <textarea class="form-control rounded-0" rows="3" name="descripcio" id="descripcio"></textarea>

                            <button type="submit" class="btn btn-primary mt-2 float-right">Afegir</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <form action="" id="deleteForm" method="post">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title text-center" style="color:white;">Eliminar Tractament</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf @method('DELETE')
                        <p class="text-center">Estas seguro que quieres eliminar el tractament?</p>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Eliminar</button>

                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function deleteData(id) {
            var id = id;
            var url = '{{ route("tractaments.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit() {
            $("#deleteForm").submit();
        }
    </script>
@stop
