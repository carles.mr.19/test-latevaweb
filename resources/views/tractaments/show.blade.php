@extends('layout.main')

@section('content')


    <div class="row justify-content-center">
        <div class="col">

            <h1 class="display-3">Tractaments</h1>

            <h2 class="display-3">{{$tractament->nom}}</h2>

            <p class="lead">{{$tractament->descripcio}}</p>

            <a href="{{route('tractaments.index')}}"><button type="button" class="btn btn-primary">Tornar</button></a>


        </div>

    </div>
@stop