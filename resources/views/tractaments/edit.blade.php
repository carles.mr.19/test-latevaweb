@extends('layout.main')

@section('content')
    <h1>Modificar Tractament</h1>

    <div class="row justify-content-center">
        <div class="col">
            <form action="{{route('tractaments.update', ['tractament' => $tractament->id])}}" method="POST">
                @csrf {{method_field('PUT')}}
                <div class="form-group">
                    <label for="nom">Nom:</label>
                    <input type="text" class="form-control" name="nom" id="nom" value="{{$tractament->nom}}" required>

                    <label for="descripcio">Descripcio</label>
                    <textarea class="form-control rounded-0" rows="3" name="descripcio" id="descripcio" required>{{$tractament->descripcio}}</textarea>


                    <a href="{{route('tractaments.index')}}"><button type="button" class="btn btn-primary mt-2">Tornar</button></a>
                    <button type="submit" class="btn btn-primary mt-2">Modificar</button>
                </div>
            </form>
        </div>
    </div>
@stop